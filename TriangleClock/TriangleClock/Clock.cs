﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace TriangleClock
{
    public class Clock
    {
        private GraphicsDevice graphicsDevice;
        private SpriteBatch spriteBatch;
        private Texture2D texture;
        private int startX;
        private int startY;

        public Clock(GraphicsDevice device, SpriteBatch batch) {
            this.spriteBatch = batch;
            graphicsDevice = device;

            texture = new Texture2D(graphicsDevice, 1, 1, false, SurfaceFormat.Color);
            texture.SetData(new[] { Color.White });

            startX = graphicsDevice.Viewport.Bounds.Width / 2;
            startY = graphicsDevice.Viewport.Bounds.Height / 2;
        }

        public void Draw() {
            drawAxis();
            drawClock();
        }

        private void drawAxis() {
            drawLine(new Vector2(startX, startY), new Vector2(startX, startY - 100));
            drawLine(new Vector2(startX, startY), new Vector2(startX + 100, startY + 100));
            drawLine(new Vector2(startX, startY), new Vector2(startX - 100, startY + 100));
        }

        private void drawClock() {
            var currentTime = DateTime.Now.TimeOfDay;
            var minutes = (100.0 / 60.0) * currentTime.Minutes;
            var seconds = (100.0 / 60.0) * currentTime.Seconds;

            var position1 = new Vector2(startX, startY - (100 / 24 * currentTime.Hours));
            var position2 = new Vector2(startX + (int)minutes, startY + (int)minutes);
            var position3 = new Vector2(startX - (int)seconds, startY + (int)seconds);

            drawLine(position1, position2);
            drawLine(position2, position3);
            drawLine(position3, position1);
        }

        private void drawLine(Vector2 point1, Vector2 point2) {
            float angle = (float)Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
            float length = Vector2.Distance(point1, point2);

            spriteBatch.Draw(texture, point1, null, Color.LightYellow, angle, Vector2.Zero, new Vector2(length, 1), SpriteEffects.None, 0);
        }
    }
}
